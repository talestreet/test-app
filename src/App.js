import './App.scss';
import Messenger from './views/Messenger';
function App() {
  return (
    <div className="app">
      <Messenger></Messenger>
    </div>
  );
}

export default App;
