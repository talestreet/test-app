import { useEffect, useState,useCallback } from "react";
import ChatList  from "./components/ChatList";
import ChatWindow from "./components/ChatWindow";
import MessengerContext from "./utils/MessengerContext";
import './Messenger.scss';
import { getChats } from "./services";
const queryString = require('query-string');
const setQueryParam=function(value){
    var searchParams = new URLSearchParams(window.location.search)
    searchParams.set("activeUser", value);
    var newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
    window.history.pushState(null, '', newRelativePathQuery);
}
const Messenger=function(){
    const [active,setActive]=useState(function(){
        const qs= queryString.parse(window.location.search);
        if(qs.activeUser){
            return qs.activeUser
        }
        return null
    })
    const [list,setList]=useState([])
    const fetchData=async function(){
        const data=await getChats()
        setList(data);
    }
    useEffect(()=>{
      fetchData()
    },[])
    const openChat=useCallback(function(id){
        setActive(id)
        setQueryParam(id)
    },[])
    return (
        <div className="messenger">
            <ChatList list={list} open={openChat}></ChatList>
            <ChatWindow id={active}></ChatWindow>
        </div>
    )
}
export default Messenger;