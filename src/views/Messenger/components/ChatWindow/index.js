import { useEffect, useState,useRef } from "react";
import { getMessages, } from "../../services";
import './ChatWindow.scss';
const ChatWindow=function({id}){
    const data=useRef(null)
    const [loading,setLoading]=useState(false);
    const [input,setInput]=useState(null)
    const fetchData=async function(){
        if(!id) return;
        const messages=await getMessages(id)
        data.current=messages;
        setLoading(false);
    }
    useEffect(()=>{
        data.current=null;
        setLoading(true);
        fetchData()
    },[id])
    const onKeyUp=function(e){
        if (e.keyCode === 13) {
            const message={
                message:input,
                id:Date.now(),
                userId:Date.now(),
                timestamp:Date.now()
            }
            // sendMessage(id,message)
        }
    }
    
    return (
        <div className="messenger__chatWindow">
            {loading && 'loading...'}
            {(!loading && !data.current) && 'Click on a user to see the messages'}
            {(!loading && data.current) && (
                <>
                <div>
                    {data.current.messages.map(({message,id:messageId,userId})=>{
                        return (
                            <div className={`chatWindow__message ${id===userId ? 'reply':''}`} key={messageId}>
                            <div className="content">
                                {message}
        
                            </div>
                            </div>
                        )
                    })}
                </div>
                <div className="chatWindow__input">
                    <input
                        value={input}
                        onChange={(e)=>{setInput(e.target.value)}}
                        onKeyUp={onKeyUp}
                    ></input>
                </div>
                </>
            )}

        </div>
    )
}
export default ChatWindow;