import './ChatList.scss';
import React, { useEffect, useState } from 'react';
const ChatList=function({list,open}){
    const [read,setRead]=useState({});
    const click=function(id){
        setRead({...read,[id]:true});
        open(id);
    }
    const [searchQuery,setSearchQuery]=useState('');
    // useEffect(()=>{

    // },[searchQuery])
    return (
        <div className='messenger__chatList'>
            <input 
                placeholder='Type to search'
                onChange={(e)=>{setSearchQuery(e.target.value)}} 
                value={searchQuery}></input>
            {list.filter(({name})=>name.toLowerCase().includes(searchQuery.toLowerCase())).map(({name,id,status})=>{
                return (
                    <div key={id} className="chatListUser" onClick={()=>{click(id)}}>
                        <div className='chatListUser__picture'></div>
                        <div> <strong>{name}</strong></div>
                        {(status==='UNREAD' && !read[id]) && '*' }
                    </div>
                )
            })}
        </div>
    )
}
export default React.memo(ChatList);