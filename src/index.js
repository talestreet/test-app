import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();



// Messenger like  designs
// Both views will be scrollable
// Left: List of chats: [Unread/active] [Searhable list]
// Right hand side: 
    // - It would a view to see the messages along with with user name or so [received/sent]
    // - Empty state if none is clicked
    //- cross button to close that
    // Input a message and add to sent entry
